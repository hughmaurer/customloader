//
//  Loader.h
//  CustomLoader
//
//  Created by Maurer on 07/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

/*!
 @header Loader
 The loader provides a class for drawing custom loader with your own bezier path.
 The loader is an abstract class.
 @copyright Hugo Maurer
 */

#import <UIKit/UIKit.h>

/*!
 *  Loader class
 */
@interface Loader : UIView

@property (nonatomic) CAShapeLayer *shapeLayer;
@property (nonatomic) CAShapeLayer *strokeLayer;
@property (nonatomic) CGPathRef path;
@property (nonatomic) CGPathRef thePath;
@property (nonatomic) UIView *loaderView;

@property (nonatomic) bool animate;
@property (nonatomic) CGFloat extraHeight;
@property (nonatomic) CGFloat oldYPoint;
@property (nonatomic)  UIColor * mainBgColor;

@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) CGFloat rectSize;
@property (nonatomic) bool swing;
@property (nonatomic) bool progressBased;

@property (nonatomic) UIColor *backgroundColor;
@property (nonatomic) UIColor *loaderColor;
@property (nonatomic) UIColor *loaderBackgroundColor;
@property (nonatomic) UIColor *loaderStrokeColor;
@property (nonatomic) CGFloat loaderStrokeWidth;
@property (nonatomic) CGFloat loaderAlpha;
@property (nonatomic) CGFloat cornerRadius;
@property (nonatomic) CGFloat progress;

+ (CGPathRef) paradiskiPath;

/*!
 *  Create the loader and show it for X secondes.
 *  Loader not in progress mode. Loader's animation go up and down.
 *
 *  @param timeInSec Loader will be visible for timeInSec secondes.
 *
 *  @return The loader created
 *
 */
+ (instancetype) showParadiskiLoaderWithTime: (int) timeInSec;

/*!
 *  Creates the loader and show it.
 *  Loader not in progress mode. Loader's animation go up and down.
 *
 *  @param path Path for creating the loader.
 *
 *  @return The loader created.
 */
+ (instancetype) showLoaderWithPath: (CGPathRef) path;

/*!
 *  Creates the loader and show it.
 *  Loader in progress mode. Progress mode change the loader's progress
 *  and start animations only when loader's progress value is changed.
 *
 *  @param path Path for creating the loader.
 *
 *  @return The loader created.
 */
+ (instancetype) showProgressBasedLoaderWithPath: (CGPathRef) path;

/*!
 *  Creates the loader but keep it hidden.
 *  Loader not in progress mode. Loader's animation go up and down.
 *
 *  @param path Path for creating the loader.
 *
 *  @return The loader created.
 */
+ (instancetype) createLoaderWithPath: (CGPathRef) path;

/*!
 *  Creates the loader but keep it hidden.
 *  Loader in progress mode. Progress mode change the loader's progress
 *  and start animations only when loader's progress value is changed.
 *
 *  @param path Path for creating the loader.
 *
 *  @return The loader created.
 */
+ (instancetype) createProgressBasedLoaderWithPath: (CGPathRef) path;

/*!
 *  Shows the loader.
 *  Call generateLoader and startAnimating methods, overriden in subclasses.
 */
- (void) showLoader;

- (void) showLoaderFor: (int) time;

/*!
 *  Creates, init and puts loader's layers together.
 *  (Masking layer, Stroke layer, Base layer and Shape layer)
 */
- (void) layoutPath;

/*!
 *  Abstract method.
 *  Generates the loader according to its loader type. (Plain, Spike, Rounded or Waves)
 *  Method overriden by subclasses.
 */
- (void) generateLoader;

/*!
 *  Abstract method.
 *  Starts animations according to its loader type. (Plain, Spike, Rounded or Waves)
 *  Method overriden by subclasses.
 */
- (void) startAnimating;

/*!
 *  Calls methods for setting up the loader properly and creating it.
 *  Methods called:
 *  - initParamValues
 *  - defaultValues
 */
- (void) initialSetup;

/*!
 *  Adds a path to the Loader. (for creating it.)
 *
 *  @param newPath The new path.
 */
- (void) addPath:(CGPathRef) newPath;

/*!
 *  Moves the loader's progress indicator, up or down depending of the parameter.
 *
 *  @param up If true, loader's progress indicator will move up. If false, it will move down.
 */
- (void) startMoving: (bool) up;

/*!
 *  Starts swinging animation.
 */
- (void) startSwinging;

/*!
 *  Stops loader animations and removes it from its superview
 */
- (void) removeLoader: (bool) animated;

@end
