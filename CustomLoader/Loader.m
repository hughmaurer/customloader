//
//  Loader.m
//  CustomLoader
//
//  Created by Maurer on 07/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "Loader.h"

@implementation Loader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (CGPathRef) paradiskiPath
{
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(82.8, 55.11)];
    [bezierPath addLineToPoint: CGPointMake(61.72, 3.5)];
    [bezierPath addLineToPoint: CGPointMake(41.71, 52.5)];
    [bezierPath addLineToPoint: CGPointMake(27.82, 29.35)];
    [bezierPath addLineToPoint: CGPointMake(15.69, 58.73)];
    [bezierPath addLineToPoint: CGPointMake(3.5, 88.27)];
    [bezierPath addLineToPoint: CGPointMake(96.33, 88.27)];
    [bezierPath addLineToPoint: CGPointMake(82.8, 55.11)];
    [bezierPath closePath];
    return bezierPath.CGPath;
}

/*
 *  Initializes Loader's variables.
 */
- (void) initParamValues
{
    _shapeLayer = [[CAShapeLayer alloc] init];
    _strokeLayer = [[CAShapeLayer alloc] init];
    _loaderView = [[UIView alloc] init];
    _animate = false;
    _extraHeight = 0;
    _oldYPoint = 0;
    _mainBgColor = [UIColor clearColor];
    _rectSize = 109.0;
    _swing = false;
    _progressBased = false;
    _oldYPoint = (_rectSize + _extraHeight) * (1 - _progress);
}

/*
 *  Sets default values of loader's variables
 */
- (void) defaultValues
{
    _duration = 10.0;
    [self setBackgroundColor:[UIColor colorWithWhite:0.2 alpha:0.4]];
    _loaderColor = [UIColor colorWithRed:0.41 green:0.728 blue:0.892 alpha:1.0];
    _loaderBackgroundColor = [UIColor whiteColor];
    _loaderStrokeColor = [UIColor blackColor];
    _loaderStrokeWidth = 0.1;
    _loaderAlpha = 1.0;
    _cornerRadius = 15.0;
    _progress = 0.0;
}

/*
 *  Custom setter of the Loader's view background color.
 *
 *  @param backgroundColor The new view background color. (default: ClearColor)
 */
- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:_mainBgColor];
    _backgroundColor = backgroundColor;
    _loaderView.backgroundColor = backgroundColor;
    _loaderView.layer.backgroundColor = backgroundColor.CGColor;
}

/*
 *  Custom setter of the Loader's color. (The "progress indicator")
 *
 *  @param loaderColor New loader color. (default: Blue)
 */
- (void)setLoaderColor:(UIColor *)loaderColor
{
    _loaderColor = loaderColor;
    _shapeLayer.fillColor = loaderColor.CGColor;
}

/*
 *  Custom setter of the Loader's background color.
 *
 *  @param loaderBackgroundColor New background color. (default: White)
 */
- (void)setLoaderBackgroundColor:(UIColor *)loaderBackgroundColor
{
    _loaderBackgroundColor = loaderBackgroundColor;
    _strokeLayer.fillColor = loaderBackgroundColor.CGColor;
}

/*
 *  Custom setter of the Loader's stroke color.
 *
 *  @param loaderStrokeColor New stroke color. (default: Black)
 */
- (void)setLoaderStrokeColor:(UIColor *)loaderStrokeColor
{
    _loaderStrokeColor = loaderStrokeColor;
    _strokeLayer.strokeColor = loaderStrokeColor.CGColor;
}

/*
 *  Custom setter of the Loader's stroke width
 *
 *  @param loaderStrokeWidth New width. (default: 0.5)
 */
- (void)setLoaderStrokeWidth:(CGFloat)loaderStrokeWidth
{
    _loaderStrokeWidth = loaderStrokeWidth;
    _strokeLayer.lineWidth = loaderStrokeWidth;
}

/*
 *  Custom setter of the Loader's alpha
 *
 *  @param loaderAlpha New alpha. (default: 1.0)
 */
- (void)setLoaderAlpha:(CGFloat)loaderAlpha
{
    _loaderAlpha = loaderAlpha;
    _loaderView.alpha = loaderAlpha;
}

/*
 *  Custom setter of the Loader's corner radius
 *
 *  @param cornerRadius New corner radius. (default: 0.0)
 */
- (void)setCornerRadius:(CGFloat)cornerRadius
{
    _cornerRadius = cornerRadius;
    _loaderView.layer.cornerRadius = cornerRadius;
}

/*
 *  Custom setter of the Loader's progress, calls applyProgress method.
 *
 *  @param progress New progress (default: 0.0)
 */
- (void)setProgress:(CGFloat)progress
{
    if (!_progressBased || progress > 1.0 || progress < 0.0)
    {
        NSLog(@"setprogress not progress based");
        return;
    }
    _progress = progress;
    [self applyProgress];
}


+  (instancetype)createLoaderWithPath:(CGPathRef)path
{
    Loader *loader = [[self alloc ]init];
    [loader initialSetup];
    [loader addPath:path];
    return loader;
}


+ (instancetype)showLoaderWithPath:(CGPathRef)path
{
    Loader *loader = [Loader createLoaderWithPath:path];
    [loader showLoader];
    return loader;
}


+ (instancetype)createProgressBasedLoaderWithPath:(CGPathRef)path
{
    Loader *loader = [[self alloc ]init];
    [loader initialSetup];
    loader.progressBased = true;
    [loader addPath:path];
    return loader;
}


+ (instancetype)showProgressBasedLoaderWithPath:(CGPathRef)path
{
    Loader *loader = [Loader createProgressBasedLoaderWithPath:path];
    [loader showLoader];
    return loader;
}


- (void) showLoader
{
    self.hidden = false;
    self.animate = true;
    [self generateLoader];
    [self startAnimating];
}

- (void)showLoaderFor:(int)time
{
    [NSTimer scheduledTimerWithTimeInterval: time
                                     target: self
                                   selector: @selector(onTick:)
                                   userInfo: nil
                                    repeats: NO];
    self.hidden = false;
    self.animate = true;
    [self generateLoader];
    [self startAnimating];
}


- (void) initialSetup
{
    // Setting up frame
    UIWindow *myWindow = [[[UIApplication sharedApplication] delegate] window];
    
    self.frame = myWindow.frame;
    self.center = CGPointMake(CGRectGetMidX(myWindow.bounds), CGRectGetMidY(myWindow.bounds));
    [myWindow setBackgroundColor: [UIColor clearColor]];
    [myWindow addSubview: self];
    
    // Setting up default values
    [self initParamValues];
    [self defaultValues];
    
    _loaderView.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y,
                                   _rectSize, _rectSize);
    _loaderView.center = CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame) / 2);
    _loaderView.layer.cornerRadius = _cornerRadius;
    
    [self addSubview:_loaderView];
    
    self.hidden = true;
}


- (void) addPath:(CGPathRef) newPath
{
    CGRect bound = CGPathGetBoundingBox(newPath);
    CGPoint center = bound.origin;
    CGFloat height = bound.size.height;
    CGFloat width = bound.size.width;
    
    CGAffineTransform transformation = CGAffineTransformMakeTranslation(-center.x - width/2 + _loaderView.frame.size.width/2,
                                                                        -center.y - height/2 + _loaderView.frame.size.height/2);
    _path = CGPathCreateCopyByTransformingPath(newPath, &transformation);
}


- (void)layoutPath
{
    CAShapeLayer *maskingLayer = [[CAShapeLayer alloc] init];
    maskingLayer.frame = _loaderView.bounds;
    maskingLayer.path = _path;
    
    // _strokeLayer = [[CAShapeLayer alloc] init];
    _strokeLayer.frame = _loaderView.bounds;
    _strokeLayer.path = _path;
    [_strokeLayer setStrokeColor: _loaderStrokeColor.CGColor];
    [_strokeLayer setLineWidth:_loaderStrokeWidth];
    [_strokeLayer setFillColor: _loaderBackgroundColor.CGColor];
    [_loaderView.layer addSublayer:_strokeLayer];
    
    CAShapeLayer *baseLayer = [[CAShapeLayer alloc] init];
    baseLayer.frame = _loaderView.bounds;
    baseLayer.mask = maskingLayer;
    
    
    // _shapeLayer = [[CAShapeLayer alloc] init];
    _shapeLayer.frame = _loaderView.bounds;
    [_shapeLayer setFillColor: _loaderColor.CGColor];
    [_shapeLayer setLineWidth:0.2];
    [_shapeLayer setStrokeColor:[UIColor blackColor].CGColor];
    CGFloat oldYpoint = _rectSize;
    _shapeLayer.position = CGPointMake(_shapeLayer.position.x, oldYpoint);
    
    [_loaderView.layer addSublayer:baseLayer];
    [baseLayer addSublayer:_shapeLayer];
}


- (void) startMoving: (bool) up
{
    if (self.progressBased)
    {
        return;
    }
    NSString *key = @"up";
    if (!up)
        key = @"down";
        CAKeyframeAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"position.y"];
        NSArray *moveUp = @[[NSNumber numberWithFloat: _loaderView.frame.size.height / 2 + _rectSize / 2],
                            [NSNumber numberWithFloat: _loaderView.frame.size.height / 2 - _rectSize / 4]];
        NSArray *moveDown = @[[NSNumber numberWithFloat: _loaderView.frame.size.height / 2 - _rectSize / 4],
                              [NSNumber numberWithFloat: _loaderView.frame.size.height / 2 + _rectSize / 2]];
        if (up)
            anim.values = moveUp;
            else
                anim.values = moveDown;
                anim.duration = _duration;
                anim.removedOnCompletion = false;
                anim.fillMode = kCAFillModeForwards;
                anim.delegate = self;
                [anim setValue:key forKey:@"animation"];
    [_shapeLayer addAnimation:anim forKey:key];
}

/*
 *  Applies the progress changes, and starts an animation for moving the loader.
 *  Animation duration can be changed by changing progressAnimation.duration. (default: 0.5)
 */
- (void) applyProgress
{
    CGFloat yPoint = (_rectSize + _extraHeight) * (1 - _progress);
    CAKeyframeAnimation *progressAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position.y"];
    NSArray *moves = @[[NSNumber numberWithFloat: _oldYPoint], [NSNumber numberWithFloat:yPoint]];
    progressAnimation.values = moves;
    progressAnimation.duration = 0.5;
    progressAnimation.removedOnCompletion = false;
    progressAnimation.fillMode = kCAFillModeForwards;
    [_shapeLayer addAnimation:progressAnimation forKey:@"progress"];
    _oldYPoint = yPoint;
}


- (void) startSwinging
{
    CAKeyframeAnimation *swingAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    NSArray *moves = @[[NSNumber numberWithFloat:0],
                       [NSNumber numberWithFloat:[self randomAngle]],
                       [NSNumber numberWithFloat:-[self randomAngle]],
                       [NSNumber numberWithFloat:[self randomAngle]],
                       [NSNumber numberWithFloat:-[self randomAngle]],
                       [NSNumber numberWithFloat:[self randomAngle]],
                       [NSNumber numberWithFloat:0]];
    swingAnimation.values = moves;
    swingAnimation.duration = 12.0;
    swingAnimation.removedOnCompletion = false;
    swingAnimation.fillMode = kCAFillModeForwards;
    swingAnimation.delegate = self;
    [swingAnimation setValue:@"rotation" forKey:@"animation"];
    [_shapeLayer addAnimation:swingAnimation forKey:@"rotation"];
}

/*
 *  Generates a random float value for returning an angle.
 *
 *  @return The random angle.
 */
- (CGFloat) randomAngle
{
    CGFloat d = M_PI_4 / (double_t)(arc4random_uniform(16) +8);
    return d;
}


- (void)generateLoader
{
    NSLog(@"generate LOADER");
}


- (void)startAnimating
{
    NSLog(@"startAnimating LOADER");
}

/*
 *  Animation delegate method for customing actions when animation completes its duration.
 *
 *  @param anim Animations stopped.
 *  @param flag Finished boolean value.
 */
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (!_animate)
        return;
    NSString *key = [anim valueForKey:@"animation"];
    if ([key  isEqual: @"up"])
        [self startMoving:false];
    if ([key  isEqual: @"down"])
        [self startMoving: true];
    if ([key isEqual: @"rotation"])
        [self startSwinging];
}

- (void) completion
{
    self.hidden = false;
    self.animate = false;
    [self removeFromSuperview];
}

- (void)removeLoader:(bool)animated
{
    if (!animated)
    {
        [self completion];
        return;
    }
    [UIView animateKeyframesWithDuration:self.duration / 10
                                   delay:0
                                 options:UIViewKeyframeAnimationOptionBeginFromCurrentState
                              animations:^{
                                  self.alpha = 0.0;}
                              completion:^(BOOL finished) {
                                  [self completion];
                              }];
}

@end