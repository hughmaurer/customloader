//
//  MovingSpikesLoader.m
//  CustomLoader
//
//  Created by Maurer on 07/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "MovingSpikesLoader.h"

@implementation MovingSpikesLoader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGFloat) randomFloat
{
    return (CGFloat)(arc4random_uniform(10) + 5);
}

- (void) startWaving
{
    CAKeyframeAnimation *waveAnimation = [CAKeyframeAnimation animationWithKeyPath:@"path"];
    waveAnimation.values = [self shapesArray:7];
    waveAnimation.duration = self.duration / 10 * 3;
    waveAnimation.removedOnCompletion = false;
    waveAnimation.fillMode = kCAFillModeForwards;
    waveAnimation.delegate = self;
    [waveAnimation setValue:@"shape" forKey:@"animation"];
    [self.shapeLayer addAnimation:waveAnimation forKey:@"shape"];
}


- (NSMutableArray*) shapesArray:(int) count
{
    NSMutableArray *shapesArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < count * 2; i += 1) {
        [shapesArray addObject: [self shapePathAtIndex:i andCount:count * 2]];
    }
    return shapesArray;
}

- (CGPathRef) shapePathAtIndex:(int) index andCount:(int) count
{
    CGFloat w_shapepath = self.loaderView.frame.size.width;
    CGFloat h_shapepath = self.loaderView.frame.size.height;
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(0, h_shapepath / 2)];
    
    CGFloat widthDiff = w_shapepath / 16;
    CGFloat nextX = widthDiff;
    CGFloat nextY = h_shapepath / 2 + _variation / 2;
    
    for (int i = 1; i <= 16; i++)
    {
        [bezierPath addLineToPoint:CGPointMake(nextX, nextY)];
        nextX += widthDiff;
        if (i % 2 == 0)
            nextY += _variation;
        else
            nextY -= _variation;
    }
    
    if (_variation <= -10)
        _incr = true;
        if (_variation >= 10)
            _incr = false;
            if (!_incr)
                _variation -= 1;
                else
                    _variation += 1;
                    self.extraHeight = abs((int)_variation);
                    
                    [bezierPath addLineToPoint: CGPointMake(w_shapepath + 100, h_shapepath / 2)];
    [bezierPath addLineToPoint: CGPointMake(w_shapepath + 100, h_shapepath * 2)];
    [bezierPath addLineToPoint: CGPointMake(0, h_shapepath * 2)];
    [bezierPath closePath];
    bezierPath.miterLimit = 4;
    bezierPath.lineWidth = 7;
    return bezierPath.CGPath;
    
}

- (void)onTick:(NSTimer *)timer
{
    [self removeLoader:true];
}

+ (instancetype)showParadiskiLoaderWithTime:(int)timeInSec
{
    MovingSpikesLoader *loader = [MovingSpikesLoader createLoaderWithPath:[self paradiskiPath]];
    loader.duration = timeInSec;
    [loader showLoaderFor:timeInSec];
    return loader;
}

+ (instancetype)createLoaderWithPath:(CGPathRef)path
{
    MovingSpikesLoader *loader = [[self alloc ]init];
    [loader initialSetup];
    [loader addPath:path];
    return loader;
}

+ (instancetype)showLoaderWithPath:(CGPathRef)path
{
    MovingSpikesLoader *loader = [MovingSpikesLoader createLoaderWithPath:path];
    [loader showLoader];
    return loader;
}

+ (instancetype)createProgressBasedLoaderWithPath:(CGPathRef)path
{
    MovingSpikesLoader *loader = [[self alloc ]init];
    [loader initialSetup];
    loader.progressBased = true;
    [loader addPath:path];
    return loader;
}

+ (instancetype)showProgressBasedLoaderWithPath:(CGPathRef)path
{
    MovingSpikesLoader *loader = [MovingSpikesLoader createProgressBasedLoaderWithPath:path];
    [loader showLoader];
    return loader;
}

- (void)generateLoader
{
    _variation = 10.0;
    _incr = false;
    self.extraHeight = 10.0;
    [self layoutPath];
}

- (void)startAnimating
{
    self.alpha = 0.0;
    [UIView animateKeyframesWithDuration:self.duration / 15
                                   delay:0
                                 options:UIViewKeyframeAnimationOptionBeginFromCurrentState
                              animations:^{
                                  self.alpha = 1.0;}
                              completion:nil];
    if (!self.animate)
        return;
    [self startWaving];
    [self startMoving: true];
}

- (void)layoutPath
{
    [super layoutPath];
    self.shapeLayer.path = [self shapePathAtIndex:0 andCount:7];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (!self.animate)
        return;
    NSString *key = [anim valueForKey:@"animation"];
    if ([key  isEqual: @"up"])
        [self startMoving:false];
    if ([key  isEqual: @"down"])
        [self startMoving: true];
    if ([key isEqual: @"rotation"])
        [self startSwinging];
    if ([key isEqual: @"shape"])
        [self startWaving];
}


@end