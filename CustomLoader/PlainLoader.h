//
//  PlainLoader.h
//  CustomLoader
//
//  Created by Maurer on 07/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

/*!
 @header PlainLoader
 A simple loader with just a line.
 @copyright Hugo Maurer
 */

#import "Loader.h"

@interface PlainLoader : Loader

/*!
 *  Generates the shape path of the plain loader
 *
 *  @return The shape path
 */
- (CGPathRef) shapePath;

@end

