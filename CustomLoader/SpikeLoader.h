//
//  SpikeLoader.h
//  CustomLoader
//
//  Created by Maurer on 07/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

/*!
 @header SpikeLoader
 A loader drawing spikes for its progress bar
 @copyright Hugo Maurer
 */

#import "Loader.h"

@interface SpikeLoader : Loader

/*!
 *  Spikes heaight
 */
@property CGFloat spikeHeight;

/*!
 *  Generates the shape path of the spike loader
 *
 *  @return The shape path
 */
- (CGPathRef) shapePath;

@end

