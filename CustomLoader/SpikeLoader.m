//
//  SpikeLoader.m
//  CustomLoader
//
//  Created by Maurer on 07/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "SpikeLoader.h"

@implementation SpikeLoader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGPathRef) shapePath
{
    CGFloat w_shapepath = self.loaderView.frame.size.width;
    CGFloat h_shapepath = self.loaderView.frame.size.height;
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(0, h_shapepath / 2)];
    
    CGFloat widthDiff = w_shapepath / 32;
    CGFloat nextX = widthDiff;
    CGFloat nextY = h_shapepath / 2 + _spikeHeight;
    
    for (int i = 1; i <= 32; i++)
    {
        [bezierPath addLineToPoint:CGPointMake(nextX, nextY)];
        nextX += widthDiff;
        if (i % 2 == 0)
            nextY += _spikeHeight;
        else
            nextY -= _spikeHeight;
    }
    
    [bezierPath addLineToPoint: CGPointMake(w_shapepath + 100, h_shapepath / 2)];
    [bezierPath addLineToPoint: CGPointMake(w_shapepath + 100, h_shapepath * 2)];
    [bezierPath addLineToPoint: CGPointMake(0, h_shapepath * 2)];
    [bezierPath closePath];
    bezierPath.miterLimit = 4;
    bezierPath.lineWidth = 7;
    return bezierPath.CGPath;
}

- (void)onTick:(NSTimer *)timer
{
    [self removeLoader:true];
}

+ (instancetype)showParadiskiLoaderWithTime:(int)timeInSec
{
    SpikeLoader *loader = [SpikeLoader createLoaderWithPath: [self paradiskiPath]];
    loader.duration = timeInSec;
    [loader showLoaderFor:timeInSec];
    return loader;
}

+ (instancetype)createLoaderWithPath:(CGPathRef)path
{
    SpikeLoader *loader = [[self alloc ]init];
    [loader initialSetup];
    loader.spikeHeight = 10.0;
    [loader addPath:path];
    return loader;
}

+ (instancetype)showLoaderWithPath:(CGPathRef)path
{
    SpikeLoader *loader = [SpikeLoader createLoaderWithPath:path];
    [loader showLoader];
    return loader;
}

+ (instancetype)createProgressBasedLoaderWithPath:(CGPathRef)path
{
    SpikeLoader *loader = [[self alloc ]init];
    [loader initialSetup];
    loader.spikeHeight = 10.0;
    loader.progressBased = true;
    [loader addPath:path];
    return loader;
}

+ (instancetype)showProgressBasedLoaderWithPath:(CGPathRef)path
{
    SpikeLoader *loader = [SpikeLoader createProgressBasedLoaderWithPath:path];
    [loader showLoader];
    return loader;
}

- (void)generateLoader
{
    self.extraHeight = _spikeHeight;
    [self layoutPath];
}

- (void)startAnimating
{
    self.alpha = 0.0;
    [UIView animateKeyframesWithDuration:self.duration / 15
                                   delay:0
                                 options:UIViewKeyframeAnimationOptionBeginFromCurrentState
                              animations:^{
                                  self.alpha = 1.0;}
                              completion:nil];
    if (!self.animate)
        return;
    if (!self.swing)
        [self startSwinging];
    [self startMoving: true];
}

- (void)layoutPath
{
    [super layoutPath];
    self.shapeLayer.path = [self shapePath];
}


@end