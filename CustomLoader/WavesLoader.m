//
//  WavesLoader.m
//  CustomLoader
//
//  Created by Maurer on 07/10/2015.
//  Copyright © 2015 Hugo.Maurer. All rights reserved.
//

#import "WavesLoader.h"

@implementation WavesLoader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
 */

- (CGFloat) randomFloat
{
    return (CGFloat)(arc4random_uniform(10) + 5);
}

- (void) startWaving
{
    CAKeyframeAnimation *waveAnimation = [CAKeyframeAnimation animationWithKeyPath:@"path"];
    waveAnimation.values = [self shapesArray:7];
    waveAnimation.duration = 2.0;
    waveAnimation.removedOnCompletion = false;
    waveAnimation.fillMode = kCAFillModeForwards;
    waveAnimation.delegate = self;
    [waveAnimation setValue:@"shape" forKey:@"animation"];
    [self.shapeLayer addAnimation:waveAnimation forKey:@"shape"];
}

- (NSMutableArray*) shapesArray:(int) count
{
    NSMutableArray *shapesArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < count * 2; i += 1) {
        
        [shapesArray addObject: [self shapePathAtIndex:i andCount:count * 2]];
    }
    return shapesArray;
}

- (CGPathRef) shapePathAtIndex:(int) index andCount:(int) count
{
    CGFloat w_shapepath = self.loaderView.frame.size.width;
    CGFloat h_shapepath = self.loaderView.frame.size.height;
    CGFloat xMovement = (w_shapepath / (CGFloat)count) * (CGFloat)index;
    bool initialOrLast = (index == 1) || (index == count);
    CGFloat divisions = 8;
    CGFloat variation = 10;
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(-w_shapepath, h_shapepath / 2)];
    
    // 1
    if (!initialOrLast)
        variation = [self randomFloat];
    [bezierPath addQuadCurveToPoint:CGPointMake(-w_shapepath + w_shapepath / 4 + xMovement, h_shapepath / 2)
                       controlPoint:CGPointMake(-w_shapepath + w_shapepath / divisions + xMovement, h_shapepath / 2 + variation)];
    if (!initialOrLast)
        variation = [self randomFloat];
    [bezierPath addQuadCurveToPoint:CGPointMake(-w_shapepath + w_shapepath / 2 + xMovement, h_shapepath / 2)
                       controlPoint:CGPointMake(-w_shapepath + w_shapepath / divisions * 3 + xMovement, h_shapepath / 2 - variation)];
    
    // 2
    if (!initialOrLast)
        variation = [self randomFloat];
    [bezierPath addQuadCurveToPoint:CGPointMake(-w_shapepath + w_shapepath / 4 * 3+ xMovement, h_shapepath / 2)
                       controlPoint:CGPointMake(-w_shapepath + w_shapepath / divisions * 5+ xMovement, h_shapepath / 2 + variation)];
    if (!initialOrLast)
        variation = [self randomFloat];
    [bezierPath addQuadCurveToPoint:CGPointMake(-w_shapepath + w_shapepath + xMovement, h_shapepath / 2)
                       controlPoint:CGPointMake(-w_shapepath + w_shapepath / divisions * 7 + xMovement, h_shapepath / 2 - variation)];
    
    // 3
    if (!initialOrLast)
        variation = [self randomFloat];
    [bezierPath addQuadCurveToPoint:CGPointMake(w_shapepath / 4 + xMovement, h_shapepath / 2)
                       controlPoint:CGPointMake(w_shapepath / divisions + xMovement, h_shapepath / 2 + variation)];
    if (!initialOrLast)
        variation = [self randomFloat];
    [bezierPath addQuadCurveToPoint:CGPointMake(w_shapepath / 2 + xMovement, h_shapepath / 2)
                       controlPoint:CGPointMake(w_shapepath / divisions * 3 + xMovement, h_shapepath / 2 - variation)];
    
    // 4
    if (!initialOrLast)
        variation = [self randomFloat];
    [bezierPath addQuadCurveToPoint:CGPointMake(w_shapepath / 4 * 3 + xMovement, h_shapepath / 2)
                       controlPoint:CGPointMake(w_shapepath / divisions * 5 + xMovement, h_shapepath / 2 + variation)];
    if (!initialOrLast)
        variation = [self randomFloat];
    [bezierPath addQuadCurveToPoint:CGPointMake(w_shapepath + xMovement, h_shapepath / 2)
                       controlPoint:CGPointMake(w_shapepath / divisions * 7 + xMovement, h_shapepath / 2 - variation)];
    
    
    [bezierPath addLineToPoint: CGPointMake(w_shapepath + 100, h_shapepath / 2)];
    [bezierPath addLineToPoint: CGPointMake(w_shapepath + 100, h_shapepath * 2)];
    [bezierPath addLineToPoint: CGPointMake(-w_shapepath, h_shapepath * 2)];
    [bezierPath closePath];
    bezierPath.miterLimit = 4;
    return bezierPath.CGPath;
}

- (void)onTick:(NSTimer *)timer
{
    [self removeLoader:true];
}

+ (instancetype)showParadiskiLoaderWithTime:(int)timeInSec
{
    WavesLoader *loader = [WavesLoader createLoaderWithPath: [self paradiskiPath]];
    loader.duration = timeInSec;
    [loader showLoaderFor:timeInSec];
    return loader;
}

+ (instancetype)createLoaderWithPath:(CGPathRef)path
{
    WavesLoader *loader = [[self alloc ]init];
    [loader initialSetup];
    [loader addPath:path];
    return loader;
}

+ (instancetype)showLoaderWithPath:(CGPathRef)path
{
    WavesLoader *loader = [WavesLoader createLoaderWithPath:path];
    [loader showLoader];
    return loader;
}

+ (instancetype)createProgressBasedLoaderWithPath:(CGPathRef)path
{
    WavesLoader *loader = [[self alloc ]init];
    [loader initialSetup];
    loader.progressBased = true;
    [loader addPath:path];
    return loader;
}

+ (instancetype)showProgressBasedLoaderWithPath:(CGPathRef)path
{
    WavesLoader *loader = [WavesLoader createProgressBasedLoaderWithPath:path];
    [loader showLoader];
    return loader;
}

- (void)generateLoader
{
    [self layoutPath];
}

- (void)startAnimating
{
    self.alpha = 0.0;
    [UIView animateKeyframesWithDuration:self.duration / 15
                                   delay:0
                                 options:UIViewKeyframeAnimationOptionBeginFromCurrentState
                              animations:^{
                                  self.alpha = 1.0;}
                              completion:nil];
    if (!self.animate)
        return;
    if (!self.swing)
        [self startSwinging];
    [self startWaving];
    [self startMoving: true];
}

- (void)layoutPath
{
    [super layoutPath];
    self.shapeLayer.path = [self shapePathAtIndex:0 andCount:7];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (!self.animate)
        return;
    NSString *key = [anim valueForKey:@"animation"];
    if ([key  isEqual: @"up"])
        [self startMoving:false];
    if ([key  isEqual: @"down"])
        [self startMoving: true];
    if ([key isEqual: @"rotation"])
        [self startSwinging];
    if ([key isEqual: @"shape"])
        [self startWaving];
}


@end